function flatten(elements) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
    let arr = [];
    let counter = 0;
    let tempArr = [];
    for (let i=0; i<elements.length; i++) {
        if (!elements[i][0]) {
            // console.log(i)
            arr.push(elements[i]);
        }
        else{
            tempArr = flatten(elements[i]);
            // console.log(tempArr);
            for (let k=0; k<tempArr.length; k++) {
                if(!tempArr[k][0]) {
                    arr.push(tempArr[k]);
                    // console.log(tempArr[k]);
                }
            }
        }
        // j++;
        // let j = 0;
        // if (elements[i+1][j]) {
        //     // console.log(elements[i]);
        //     tempArr = flatten(elements[i]);
        //     arr.push(tempArr);
            
        //     // for (let k=0; k<tempArr.length; k++) {
        //     //     arr.push(tempArr[k]);
        //     //     // console.log(tempArr[k]);
        //     //     // counter++;
        // }
        // else {
        //     // return elements[i];
        //     // arr.push(elements[i]);
        // }
        // j++;
        // counter++;
    }
    return arr;
}

module.exports = flatten;